package br.com.brunokawakami.daggerinjectionexample.Core.Injection

import android.app.Application
import android.content.Context
import android.location.LocationManager
import br.com.brunokawakami.daggerinjectionexample.BuildConfig
import br.com.brunokawakami.daggerinjectionexample.Core.Abstracts.TaskInterface
import br.com.brunokawakami.daggerinjectionexample.Core.Providers.SnackbarMessage
import br.com.brunokawakami.daggerinjectionexample.Domains.Main.Tasks.MainMock
import br.com.brunokawakami.daggerinjectionexample.Domains.Main.Tasks.MainTask

import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class MockModule(private val application: Application) {

    @Provides
    @Singleton
    @ForApplication
    fun provideApplicationContext(): Context {
        return application
    }


    @Provides
    @Singleton
    fun provideLocationManager(): LocationManager {
        return application.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    @Provides
    @Singleton
    fun provideSnackbarMessage(): SnackbarMessage {
        return SnackbarMessage()
    }

    @Provides
    @Singleton
    fun provideButtonText(): TaskInterface {
        if (BuildConfig.DEBUG) return MainMock() else return MainTask()
    }

}