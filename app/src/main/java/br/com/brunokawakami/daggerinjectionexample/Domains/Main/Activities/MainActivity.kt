package br.com.brunokawakami.daggerinjectionexample.Domains.Main.Activities

import android.location.LocationManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import br.com.brunokawakami.daggerinjectionexample.Core.Abstracts.TaskInterface
import br.com.brunokawakami.daggerinjectionexample.Core.App.MainApplication
import br.com.brunokawakami.daggerinjectionexample.Core.Providers.SnackbarMessage
import br.com.brunokawakami.daggerinjectionexample.R
import butterknife.ButterKnife
import butterknife.bindOptionalView
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    val TAG = MainActivity::class.java.name

    val button: Button? by bindOptionalView(R.id.buttonTest)

    @Inject
    lateinit var locationManager: LocationManager

    @Inject
    lateinit var snackbarMessage: SnackbarMessage

    @Inject
    lateinit var buttonText: TaskInterface


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainApplication.graph.inject(this)
        ButterKnife.bind(this)
        snackbarMessage.show(this, "Mesagi")
        button?.text = buttonText.getText()
    }

}