package br.com.brunokawakami.daggerinjectionexample.Core.App


import android.app.Application
import android.location.LocationManager
import br.com.brunokawakami.daggerinjectionexample.BuildConfig
import br.com.brunokawakami.daggerinjectionexample.Core.Injection.MockModule
import br.com.brunokawakami.daggerinjectionexample.Core.Injection.AndroidModule
import br.com.brunokawakami.daggerinjectionexample.Core.Injection.ApplicationComponent
import br.com.brunokawakami.daggerinjectionexample.Core.Injection.DaggerApplicationComponent
import javax.inject.Inject
/**
 * Created by loop on 09/12/14.
 */

class MainApplication() : Application() {

    companion object {
        @JvmStatic lateinit var graph: ApplicationComponent
    }


    override fun onCreate() {
        super.onCreate()

            graph = DaggerApplicationComponent
                    .builder()
                    .mockModule(MockModule(this))
                    .build()
            return


    }


}