package br.com.brunokawakami.daggerinjectionexample.Core.Injection

import br.com.brunokawakami.daggerinjectionexample.Domains.Main.Activities.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(MockModule::class))
interface ApplicationComponent {

    fun inject(mainActivity: MainActivity)
}