package br.com.brunokawakami.daggerinjectionexample.Core.Injection


import javax.inject.Qualifier


/**
 * Created by loop on 09/12/14.
 */

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ForApplication