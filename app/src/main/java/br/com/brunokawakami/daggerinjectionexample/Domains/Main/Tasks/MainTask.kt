package br.com.brunokawakami.daggerinjectionexample.Domains.Main.Tasks

import br.com.brunokawakami.daggerinjectionexample.Core.Abstracts.TaskInterface

/**
 * Created by bruno on 11/24/16.
 */
class MainTask : TaskInterface {
    override fun getText(): String {
        return "Botao prod"
    }

}