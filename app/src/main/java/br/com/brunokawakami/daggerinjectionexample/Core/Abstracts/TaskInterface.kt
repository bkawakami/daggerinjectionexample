package br.com.brunokawakami.daggerinjectionexample.Core.Abstracts

/**
 * Created by bruno on 11/24/16.
 */
interface TaskInterface {
    fun getText(): String

}