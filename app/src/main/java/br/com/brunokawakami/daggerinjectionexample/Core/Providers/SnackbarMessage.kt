package br.com.brunokawakami.daggerinjectionexample.Core.Providers

import android.app.Activity
import android.support.design.widget.Snackbar
import android.view.View

/**
 * Created by bruno on 11/23/16.
 */

class SnackbarMessage () {
    fun show(activity: Activity, message: String){
        val snackbar = Snackbar.make(activity.window.decorView, message, Snackbar.LENGTH_LONG).show()
    }
}